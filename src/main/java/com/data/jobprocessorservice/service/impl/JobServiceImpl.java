package com.data.jobprocessorservice.service.impl;

import com.data.jobprocessorservice.constant.JobServiceConstants;
import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

@Slf4j
@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private WebClient webClient;

    @Override
    public List<JobPosition> getAllJobs() throws JobServiceErrorResponse {

        log.info("Fetching all jobs available from job-service.");

        try {
            return webClient.get().uri(JobServiceConstants.GET_ALL_JOBS_V1)
                    .retrieve()
                    .bodyToFlux(JobPosition.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new JobServiceErrorResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}" , ex.getMessage());
            throw new JobServiceErrorResponse(ex);
        }

    }

    @Override
    public JobPosition getPositionWithId(Long id) throws JobServiceErrorResponse {

        log.info("Getting job from job-service with id : {} ", id);

        try {
            return webClient.get().uri(JobServiceConstants.GET_JOBS_WITH_ID_V1,id)
                    .retrieve()
                    .bodyToMono(JobPosition.class)
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new JobServiceErrorResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}" , ex.getMessage());
            throw new JobServiceErrorResponse(ex);
        }
    }

    @Override
    public List<JobPosition> createJobPositions(List<JobPosition> requestJobPositionList) throws JobServiceErrorResponse {

        log.info("posting jobs to job-service with : {} ", requestJobPositionList);

        try {
            return webClient.post().uri(JobServiceConstants.CREATE_V1)
                    .body(BodyInserters.fromValue(requestJobPositionList))
                    .retrieve()
                    .bodyToFlux(JobPosition.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new JobServiceErrorResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}" , ex.getMessage());
            throw new JobServiceErrorResponse(ex);
        }
    }

    @Override
    public List<JobPosition> updateJobPositions(List<JobPosition> requestJobPositionList) throws JobServiceErrorResponse {

        log.info("updating jobs to job-service with : {} ", requestJobPositionList);

        try {
            return webClient.post().uri(JobServiceConstants.CREATE_V1)
                    .body(BodyInserters.fromValue(requestJobPositionList))
                    .retrieve()
                    .bodyToFlux(JobPosition.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException ex) {
            log.error("Error occurred while calling job-service API. {}, {}", ex.getStatusText(), ex.getResponseBodyAsString());
            throw new JobServiceErrorResponse(ex.getStatusText(), ex);
        } catch (Exception ex) {
            log.error("Exception occurred , {}" , ex.getMessage());
            throw new JobServiceErrorResponse(ex);
        }

    }
}
