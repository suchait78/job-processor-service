package com.data.jobprocessorservice.service.impl;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.model.Position;
import com.data.jobprocessorservice.repository.PositionRepository;
import com.data.jobprocessorservice.service.WiremockDbIntegrationService;
import com.data.jobprocessorservice.util.JobPositionUtil;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class WiremockH2IntegrationServiceImpl implements WiremockDbIntegrationService {

    @Autowired
    private PositionRepository positionRepository;

    @Override
    public List<JobPosition> getAllJobs() {

        List<Position> positionsList = positionRepository.findAll();

        /*
        List<Position> positionsList = new ArrayList<>();

        positionsList.add(new Position("Analyst", "Description-1", "Pune", 10L, "Company-1"));
        positionsList.add(new Position("Developer", "Description-2", "Gurgaon", 10L, "Company-12"));
        positionsList.add(new Position("Business Analyst", "Description-3", "Bangalore", 10L, "Company-19"));
        positionsList.add(new Position("Manager", "Description-4", "Pune", 10L, "Company-17"));
        positionsList.add(new Position("Technical Support", "Description-5", "Pune", 10L, "Company-100"));
         */

        List<JobPosition> jobPositionList =
                positionsList.stream()
                        .map(position -> JobPositionUtil.convert(position))
                        .collect(Collectors.toList());

        return jobPositionList;
    }

    @Override
    public JobPosition getPositionWithId(Long id) {
        return null;
    }

    @Override
    public List<JobPosition> createJobPositions(List<JobPosition> requestJobPositionList) {
        return null;
    }

    @Override
    public List<JobPosition> updateJobPositions(List<JobPosition> requestJobPositionList) {
        return null;
    }
}
