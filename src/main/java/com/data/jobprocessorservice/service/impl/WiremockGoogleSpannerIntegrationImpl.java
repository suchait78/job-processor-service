package com.data.jobprocessorservice.service.impl;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.service.WiremockDbIntegrationService;

import java.util.List;

public class WiremockGoogleSpannerIntegrationImpl implements WiremockDbIntegrationService {

    @Override
    public List<JobPosition> getAllJobs() {
        return null;
    }

    @Override
    public JobPosition getPositionWithId(Long id) {
        return null;
    }

    @Override
    public List<JobPosition> createJobPositions(List<JobPosition> requestJobPositionList) {
        return null;
    }

    @Override
    public List<JobPosition> updateJobPositions(List<JobPosition> requestJobPositionList) {
        return null;
    }
}
