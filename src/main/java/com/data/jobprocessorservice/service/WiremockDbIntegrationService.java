package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.dto.JobPosition;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface WiremockDbIntegrationService {

    public List<JobPosition> getAllJobs();
    public JobPosition getPositionWithId(Long id);
    public List<JobPosition> createJobPositions(List<JobPosition> requestJobPositionList);
    public List<JobPosition> updateJobPositions(List<JobPosition> requestJobPositionList);

}
