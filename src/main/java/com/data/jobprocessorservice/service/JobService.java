package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;

import java.util.List;

public interface JobService {

    public List<JobPosition> getAllJobs() throws JobServiceErrorResponse;
    public JobPosition getPositionWithId(Long id) throws JobServiceErrorResponse;
    public List<JobPosition> createJobPositions(List<JobPosition> requestJobPositionList) throws JobServiceErrorResponse;
    public List<JobPosition> updateJobPositions(List<JobPosition> requestJobPositionList) throws JobServiceErrorResponse;

}
