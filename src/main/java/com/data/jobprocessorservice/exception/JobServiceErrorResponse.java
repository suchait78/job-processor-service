package com.data.jobprocessorservice.exception;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class JobServiceErrorResponse extends Exception{

    public JobServiceErrorResponse(String message, WebClientResponseException ex) {
        super(message, ex);
    }

    public JobServiceErrorResponse(Exception ex) {
        super(ex);
    }
}
