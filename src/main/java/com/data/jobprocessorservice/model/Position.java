package com.data.jobprocessorservice.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long jobId;
    private String jobName;
    private String jobDescription;
    private String jobLocation;
    private Long positions;
    private String organizationName;

    public Position(String jobName, String jobDescription, String jobLocation, Long positions, String organizationName) {
        this.jobName = jobName;
        this.jobDescription = jobDescription;
        this.jobLocation = jobLocation;
        this.positions = positions;
        this.organizationName = organizationName;
    }

}
