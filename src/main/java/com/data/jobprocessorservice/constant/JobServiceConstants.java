package com.data.jobprocessorservice.constant;

public class JobServiceConstants {

    public static final String GET_ALL_JOBS_V1 = "/all/positions";
    public static final String GET_JOBS_WITH_ID_V1 = "/position/{id}";
    public static final String CREATE_V1 = "/create";
    public static final String UPDATE_V1 = "/update";

}
