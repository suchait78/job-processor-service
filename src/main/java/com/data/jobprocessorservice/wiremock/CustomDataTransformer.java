package com.data.jobprocessorservice.wiremock;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.service.WiremockDbIntegrationService;
import com.data.jobprocessorservice.util.JsonUtil;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import wiremock.net.minidev.json.JSONObject;

import java.util.List;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Component
@Setter
public class CustomDataTransformer extends ResponseDefinitionTransformer {

    private WiremockDbIntegrationService wiremockDbIntegrationService;

    private List<JobPosition> getData() {
        log.info("Executing getData() method.");
        return wiremockDbIntegrationService.getAllJobs();
    }

    @SneakyThrows
    @Override
    public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource, Parameters parameters) {


        log.info("Inside transform() method.");

        String finalList = JsonUtil.getJSONdata(getData());

        return new ResponseDefinitionBuilder()
                .withStatus(HttpStatus.OK.value())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .withBody(finalList)
                .build();


    }

    @Override
    public String getName() {
        return "CustomDataTransformer";
    }
}
