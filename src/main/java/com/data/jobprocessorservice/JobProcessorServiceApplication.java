package com.data.jobprocessorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.data.jobprocessorservice.model")
public class JobProcessorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobProcessorServiceApplication.class, args);
	}

}
