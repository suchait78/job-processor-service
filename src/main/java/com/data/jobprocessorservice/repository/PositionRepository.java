package com.data.jobprocessorservice.repository;

import com.data.jobprocessorservice.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface PositionRepository extends JpaRepository<Position, Long> {

}
