package com.data.jobprocessorservice.util;

import com.data.jobprocessorservice.dto.JobPosition;
import lombok.extern.slf4j.Slf4j;
import wiremock.net.minidev.json.JSONArray;
import wiremock.net.minidev.json.JSONObject;


import java.util.List;

@Slf4j
public class JsonUtil {

    public static String getJSONdata(List<JobPosition> jobPositionList) {

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        for(JobPosition jobPosition : jobPositionList) {

            JSONObject object = new JSONObject();
            object.put("job_id",jobPosition.getJobId());
            object.put("job_name",jobPosition.getJobName());
            object.put("job_description",jobPosition.getJobDescription());
            object.put("job_location",jobPosition.getJobLocation());
            object.put("positions",jobPosition.getPositions());
            object.put("organization_name",jobPosition.getOrganizationName());

           jsonArray.add(object);
        }

        jsonObject.put("job_position_list",jsonArray);
        String jsonString = jsonObject.toString();
        log.info("JSON string created : {} ", jsonString);

        return jsonString;
    }
}
