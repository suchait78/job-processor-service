package com.data.jobprocessorservice.util;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.model.Position;

public class JobPositionUtil {

    public static JobPosition convert(Position position) {

        JobPosition jobPosition = new JobPosition.JobPositionBuilder(position.getJobId())
                .withJobName(position.getJobName())
                .withJobPositions(position.getPositions())
                .withJobDescription(position.getJobDescription())
                .withJobLocation(position.getJobLocation())
                .withOrganizationName(position.getOrganizationName())
                .build();

        return jobPosition;
    }
}
