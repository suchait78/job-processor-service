package com.data.jobprocessorservice.removelater;

import com.data.jobprocessorservice.model.Position;
import com.data.jobprocessorservice.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RemoveMeLaterClass {

    @Autowired
    private PositionRepository positionRepository;

    public void createData() {

        List<Position> positions = new ArrayList<>();

        positions.add(new Position(12L, "Analyst", "Description-1", "Pune", 10L, "Company-1"));
        positions.add(new Position(13L, "Developer", "Description-2", "Gurgaon", 10L, "Company-12"));
        positions.add(new Position(14L, "Business Analyst", "Description-3", "Bangalore", 10L, "Company-19"));
        positions.add(new Position(15L, "Manager", "Description-4", "Pune", 10L, "Company-17"));
        positions.add(new Position(16L, "Technical Support", "Description-5", "Pune", 10L, "Company-100"));

        positionRepository.saveAll(positions);
    }

}
