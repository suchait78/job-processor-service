package com.data.jobprocessorservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class JobPosition {

    @JsonProperty("job_id")
    private Long jobId;

    @JsonProperty("job_name")
    private String jobName;

    @JsonProperty("job_description")
    private String jobDescription;

    @JsonProperty("job_location")
    private String jobLocation;

    @JsonProperty("positions")
    private Long positions;

    @JsonProperty("organization_name")
    private String organizationName;

    public static class JobPositionBuilder {

        private Long jobId;
        private String jobName;
        private String jobDescription;
        private String jobLocation;
        private Long positions;
        private String organizationName;

        public JobPositionBuilder(Long jobId) {
            this.jobId = jobId;
        }

        public JobPositionBuilder withJobName(String jobName) {
            this.jobName = jobName;
            return this;
        }

        public JobPositionBuilder withJobDescription(String jobDescription) {
            this.jobDescription = jobDescription;
            return this;
        }

        public JobPositionBuilder withJobLocation(String jobLocation) {
            this.jobLocation = jobLocation;
            return this;
        }

        public JobPositionBuilder withJobPositions(Long positions) {
            this.positions = positions;
            return this;
        }

        public JobPositionBuilder withOrganizationName(String organizationName) {
            this.organizationName = organizationName;
            return this;
        }

        public JobPosition build() {

            JobPosition jobPosition = new JobPosition();
            jobPosition.setJobId(this.jobId);
            jobPosition.setPositions(this.positions);
            jobPosition.setJobDescription(this.jobDescription);
            jobPosition.setJobLocation(this.jobLocation);
            jobPosition.setJobName(this.jobName);

            return jobPosition;
        }

    }

}
