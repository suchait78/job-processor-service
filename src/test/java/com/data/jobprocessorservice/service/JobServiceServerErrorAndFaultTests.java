package com.data.jobprocessorservice.service;


import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.service.impl.JobServiceImpl;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;


import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8088"})
public class JobServiceServerErrorAndFaultTests {

    Options options = wireMockConfig().
            port(8088)
            .notifier(new ConsoleNotifier(true))
            .extensions(new ResponseTemplateTransformer(true));

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options);

    @Autowired
    private JobServiceImpl jobService;


    @Test
    public void getAllJobsTest() throws JobServiceErrorResponse {

        stubFor(get(anyUrl())
                .willReturn(serverError()));

        Assert.assertThrows(JobServiceErrorResponse.class, () -> jobService.getAllJobs());
    }


    @Test
    public void getAllJobsTests_WithServiceUnavailable() throws JobServiceErrorResponse {

        stubFor(get(anyUrl())
                .willReturn(serverError()
                        .withStatus(HttpStatus.SERVICE_UNAVAILABLE.value())
                        .withBody("Service Unavailable")));

       JobServiceErrorResponse jobServiceErrorResponse =
               Assertions.assertThrows(JobServiceErrorResponse.class, () -> jobService.getAllJobs());
        assertEquals("Service Unavailable", jobServiceErrorResponse.getMessage());
    }

    @Test
    public void getAllJobsTest_withNetworkRelatedError() {

        stubFor(get(anyUrl())
                .willReturn(aResponse().withFault(Fault.EMPTY_RESPONSE)));

        JobServiceErrorResponse jobServiceErrorResponse =
                Assertions.assertThrows(JobServiceErrorResponse.class, () -> jobService.getAllJobs());


        String errorMessage = "org.springframework.web.reactive.function.client.WebClientRequestException: Connection prematurely closed BEFORE response; nested exception is reactor.netty.http.client.PrematureCloseException: Connection prematurely closed BEFORE response";
        assertEquals(errorMessage, jobServiceErrorResponse.getMessage());
    }

    @Test
    public void getAllJobsTest_withTimeout() {

        stubFor(get(anyUrl())
                .willReturn(ok().withFixedDelay(10000)));

        Assertions.assertThrows(JobServiceErrorResponse.class, () -> jobService.getAllJobs());
    }

}
