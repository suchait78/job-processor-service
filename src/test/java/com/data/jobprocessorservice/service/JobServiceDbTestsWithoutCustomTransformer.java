package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.removelater.RemoveMeLaterClass;
import com.data.jobprocessorservice.service.impl.JobServiceImpl;
import com.data.jobprocessorservice.service.impl.WiremockH2IntegrationServiceImpl;
import com.data.jobprocessorservice.util.JsonUtil;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import wiremock.net.minidev.json.JSONObject;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8088"})
public class JobServiceDbTestsWithoutCustomTransformer {


    Options options = wireMockConfig().
            port(8088)
            .notifier(new ConsoleNotifier(true));

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options);

    @Autowired
    private WiremockH2IntegrationServiceImpl wiremockH2IntegrationService;

    @Autowired
    private JobServiceImpl jobService;

    @Autowired
    private RemoveMeLaterClass removeMeLaterClass;

    @Test
    public void testGetAllJobsWithInternalStub() throws JobServiceErrorResponse {

        removeMeLaterClass.createData();

        List<JobPosition> jobPositionList = wiremockH2IntegrationService.getAllJobs();
        String jsonObject = JsonUtil.getJSONdata(jobPositionList);

        stubFor(get(anyUrl())
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(jsonObject)));

        List<JobPosition> list = jobService.getAllJobs();

        log.info("Data Received from database : {}", list);

        Assert.assertTrue(list.size() > 0);


    }

}
