package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.service.impl.JobServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8077"})
public class JobServiceBasicTests {

    @Autowired
    private JobServiceImpl jobService;

    @Test
    public void testJobService() throws JobServiceErrorResponse {

        List<JobPosition> list = jobService.getAllJobs();
        Assert.assertNotNull(list);
    }

    @Test
    public void testJobServiceWithId() throws JobServiceErrorResponse {

        JobPosition pos = jobService.getPositionWithId(19L);
        log.info(String.valueOf(pos));
        Assert.assertNotNull(pos);
    }

    @Test
    public void testJobServiceCreate() throws JobServiceErrorResponse {

        List<JobPosition> positionList = new ArrayList<>();

        positionList.add(new JobPosition(101L, "CTO", "Description-1", "Bangalore", 2L, "Company-1"));
        positionList.add(new JobPosition(102L, "CEO", "Description-2", "Bangalore", 2L, "Company-12"));

        List<JobPosition> jobPositionList = jobService.createJobPositions(positionList);
        log.info(String.valueOf(jobPositionList));
        Assert.assertNotNull(jobPositionList);
    }

}
