package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.constant.JobServiceConstants;
import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.service.impl.JobServiceImpl;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8088"})
//@AutoConfigureWireMock(port = 8088)
public class JobServiceWireMockTests {

    Options options = wireMockConfig().
            port(8088)
            .notifier(new ConsoleNotifier(true))
            .extensions(new ResponseTemplateTransformer(true));

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options);

    @Autowired
    private JobServiceImpl jobService;

    @Test
    public void getAllJobsTest() throws JobServiceErrorResponse {

        stubFor(get(anyUrl())
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("all-jobs.json")));

        List<JobPosition> list = jobService.getAllJobs();
        Assert.assertTrue(list.size() > 0);
    }

    @Test
    public void getJobWithIdTest() throws JobServiceErrorResponse {

        stubFor(get(urlPathMatching("/position/[0-9]"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("single-job.json")));
        Long id = 8L;

        JobPosition pos = jobService.getPositionWithId(id);
        log.info(String.valueOf(pos));
        Assert.assertNotNull(pos);
    }


    @Test
    public void getJobWithId_WithResponseTemplatingTest() throws JobServiceErrorResponse {

        stubFor(get(urlPathMatching("/position/[0-9]"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("single-job-template.json")));
        Long id = 5L;

        JobPosition pos = jobService.getPositionWithId(id);
        log.info(String.valueOf(pos));
        Assert.assertNotNull(pos);
    }

    @Test(expected = JobServiceErrorResponse.class)
    public void getJobWithExceptionTest() throws JobServiceErrorResponse {

        stubFor(get(urlPathMatching("/position/[0-9]"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("404-job-service.json")));

        Long id = 5L;

        JobPosition pos = jobService.getPositionWithId(id);
        log.info(String.valueOf(pos));

    }

    @Test
    public void createJobsTest() throws JobServiceErrorResponse {

        List<JobPosition> positionList = new ArrayList<>();

        positionList.add(new JobPosition(101L, "CTO", "Description-1", "Bangalore", 2L, "Company-1"));
        positionList.add(new JobPosition(102L, "CEO", "Description-2", "Bangalore", 2L, "Company-12"));

        stubFor(post(urlPathEqualTo(JobServiceConstants.CREATE_V1))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("create-jobs.json")));

        List<JobPosition> jobPositionList = jobService.createJobPositions(positionList);
        log.info(String.valueOf(jobPositionList));
        Assert.assertNotNull(jobPositionList);

    }

    @Test(expected = JobServiceErrorResponse.class)
    public void createJobs_WithExceptionsTests() throws JobServiceErrorResponse {

        List<JobPosition> positionList = new ArrayList<>();

        stubFor(post(urlPathEqualTo(JobServiceConstants.CREATE_V1))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.BAD_REQUEST.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("empty-create-jobs.json")));

        List<JobPosition> jobPositionList = jobService.createJobPositions(positionList);

    }


}
