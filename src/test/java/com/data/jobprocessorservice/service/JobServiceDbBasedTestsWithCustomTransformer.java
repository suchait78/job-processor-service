package com.data.jobprocessorservice.service;

import com.data.jobprocessorservice.JobProcessorServiceApplication;
import com.data.jobprocessorservice.model.Position;
import com.data.jobprocessorservice.removelater.RemoveMeLaterClass;
import com.data.jobprocessorservice.dto.JobPosition;
import com.data.jobprocessorservice.exception.JobServiceErrorResponse;
import com.data.jobprocessorservice.repository.PositionRepository;
import com.data.jobprocessorservice.service.impl.JobServiceImpl;
import com.data.jobprocessorservice.service.impl.WiremockGoogleSpannerIntegrationImpl;
import com.data.jobprocessorservice.service.impl.WiremockH2IntegrationServiceImpl;
import com.data.jobprocessorservice.wiremock.CustomDataTransformer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.config.BootstrapMode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"job.service.base.url=http://localhost:8088"})
public class JobServiceDbBasedTestsWithCustomTransformer {

    @Autowired
    private WiremockH2IntegrationServiceImpl wiremockH2IntegrationService;

    private CustomDataTransformer customDataTransformer = new CustomDataTransformer();

    Options options = wireMockConfig().
            port(8088)
            .notifier(new ConsoleNotifier(true))
            .extensions(customDataTransformer);

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options);

    @Autowired
    private JobServiceImpl jobService;

    @Autowired
    private RemoveMeLaterClass removeMeLaterClass;

    @Test
    public void testGetAllJobs() throws JobServiceErrorResponse {

        removeMeLaterClass.createData();
        customDataTransformer.setWiremockDbIntegrationService(wiremockH2IntegrationService);

        stubFor(get(anyUrl())
                .willReturn(aResponse().withTransformers("CustomDataTransformer")));

        List<JobPosition> list = jobService.getAllJobs();

        log.info("Data Received from database : {}", list);

        Assert.assertTrue(list.size() > 0);


    }

}
